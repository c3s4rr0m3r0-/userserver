# DeveloperTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.5.

## Download nodes

Run `npm i` to download the nodes`.


## Development server

Run `npm run json` for a dev server. Navigate to `http://localhost:3000/`. The server will automatically reload if you change any of the source files.